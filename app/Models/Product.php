<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'product',
        'condition',
        'segment',
        'description',
        'img_URL',

    ];

    /**
     * Get the user that belong to the product
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
