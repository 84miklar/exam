<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    public $segmentType = "";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 50) as $value) {
            DB::table('products')->insert([
                'segment' => $this->addSegment(),
                'product' => $this->addProduct(),
                'price' => $faker->numberBetween($min = 1000, $max = 20000),
                'condition' => $this->addCondition(),
                'description' => $faker->text($maxNbChars = 400),
                'img_URL' => $this->addImgURL('segment'),
                'user_id' => rand(1, 50),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
    /**
     * Used for setting a product name that fits the segment.
     */
    private function addProduct()
    {
        $product = "Ingen titel";
        $this->segmentType == 'Hem och Fritid' ?  $product = 'Grym mountainbike' :  $product;
        $this->segmentType == 'Elektronik' ?  $product = 'Cool dator' :  $product;
        $this->segmentType == 'Fordon' ?  $product = 'Fräsch bil' :  $product;
        return $product;
    }
    /**
     * Sets the condition of the product.
     */
    private function addCondition()
    {
        $condition = '';
        $random = rand(1, 3);
        if ($random == 1) {
            $condition = 'använt';
        } elseif ($random == 2) {
            $condition = 'bra';
        } else {
            $condition = 'som ny';
        }
        return $condition;
    }
    /**
     * Sets the segment of the product.
     */
    private function addSegment()
    {
        $segment = '';
        $random = rand(1, 3);
        if ($random == 1) {
            $segment = 'Hem och Fritid';
        } elseif ($random == 2) {
            $segment = 'Elektronik';
        } else {
            $segment = 'Fordon';
        }
        $this->segmentType = $segment;
        return $segment;
    }
    /**
     *Sets an image that fits the segment
     */
    private function addImgURL()
    {
        $img_URL = "logoSquare.png";
        $this->segmentType == 'Hem och Fritid' ? $img_URL = 'mountainbike.jpg' : $img_URL;
        $this->segmentType == 'Elektronik' ? $img_URL = 'computer.jpg' : $img_URL;
        $this->segmentType == 'Fordon' ? $img_URL = 'usedcar.jpg' : $img_URL;
        return $img_URL;
    }
}
