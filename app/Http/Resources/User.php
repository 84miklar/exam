<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     * Make your own data output at api requests.
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'municipality' => $this->municipality,
            'telephone' => $this->telephone,
            'is_admin' => $this->is_admin,
        ];
    }
    public function with($request)
    {
        return [
            'version' => '1.0.0',
            'developer_name' => 'Mikael Larsson',
            'developer_url' => 'http://mikaellarsson.netlify.com'
        ];
    }
}
