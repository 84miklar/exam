<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    /** 
     *  This method handles the applications upload image requests.
     *  @param $request
     * @return string the file name stored in storage/app/public/images
     */
    //source: https://www.youtube.com/watch?v=AL8PCThJ9c4&t=910s
    public function imageUpload(Request $request)
    {
        if ($request != null) {

            //validate that the request is a valid image.
            $request->validate([

                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            ]);
        }

        if ($request->hasFile('image')) {
            //get the original filname     
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('image')->getClientOriginalExtension();
            // filename to store with timestamp for uniqe value
            $filenameToStore = $filename . '_' . time() . "." . $extension;

            //Upload image file to storage
            $request->file('image')->storeAs('public/images', $filenameToStore);
            return $filenameToStore;
        }
    }
}
