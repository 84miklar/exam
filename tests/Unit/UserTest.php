<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserTest extends TestCase
{
    private $user = null;
    public function test_store_and_delete_new_user()
    {
        $this->user = User::factory()->count(1)->create();
        $userToDelete = User::findOrFail($this->user[0]['id']);
        $userToDelete->delete();
        $this->assertDatabaseMissing('users', ['email' => $this->user[0]['email']]);
    }
}
