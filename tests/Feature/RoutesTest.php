<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoutesTest extends TestCase
{

    public function test_api_users()
    {
        $response = $this->get('/api/users');

        $response->assertStatus(200);
    }
    public function test_api_users_get_user()
    {
        $response = $this->get('/api/user/50');

        $response->assertStatus(200);
    }
    public function test_api_users_product_count()
    {
        $response = $this->get('/api/users/productcount');

        $response->assertStatus(200);
    }
    public function test_api_products()
    {
        $response = $this->get('/api/products');

        $response->assertStatus(200);
    }
    public function test_api_products_category()
    {
        $cat = "1";
        $response = $this->get('/api/products/' . $cat);
        $response->assertStatus(200);
    }
    public function test_api_products_category_bad_request()
    {
        $cat = "bad request";
        $response = $this->get('/api/products/' . $cat);
        $response->assertStatus(404);
    }
    public function test_api_products_search()
    {
        $searchValue = "bil";
        $response = $this->get('/api/products/search/' . $searchValue);
        $response->assertStatus(200);
    }
}
