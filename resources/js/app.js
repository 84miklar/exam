// This loads the Vue component mainapp

require("./bootstrap");

import Vue from "vue";
import VueRouter from "vue-router";
import router from "./router";
import mainapp from "./vue/mainapp.vue";
import { store } from "./store";

Vue.use(VueRouter);

const app = new Vue({
    el: "#app",
    components: { mainapp },
    store,
    router,
});
