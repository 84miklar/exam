<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => $this->product,
            'price' => $this->price,
            'segment' => $this->segment,
            'description' => $this->description,
            'img_URL' => $this->img_URL,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ];
    }
    public function with($request)
    {
        return [
            'version' => '1.0.0',
            'developer_name' => 'Mikael Larsson',
            'developer_url' => 'http://mikaellarsson.netlify.com'
        ];
    }
}
