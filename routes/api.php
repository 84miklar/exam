<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ImageUploadController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//If request is being sent to /user, an authenticated user is being sent back, or error.
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


/**
 * Users routing
 */
//list users
Route::get('users', [UserController::class, 'index']);

// Update a user
Route::put('update', [UserController::class, 'update']);

//List user by Id
Route::get('user/{id}', [UserController::class, 'show']);

//Create a new user
Route::post('user/store', [UserController::class, 'store']);

// Delete a user by Id
Route::delete('user/{id}', [UserController::class, 'destroy']);

// Login a registered user
Route::post('login', [UserController::class, 'login'])->name("login");

// Logout a logged in user
Route::get('logout', [UserController::class, 'logout'])->name("logout");

//List users ordered by amount of products
Route::get('users/productcount', [UserController::class, 'getTheMostSeller']);



/**
 * Products routing
 */

//list products
Route::get('products', [ProductController::class, 'index']);

//list products in chosen category
Route::get('products/{category}', [ProductController::class, 'getCategory']);

//List products by userId
Route::get('product/user/{userId}', [ProductController::class, 'showUserProducts']);

//Create a new product
Route::post('product', [ProductController::class, 'store']);

//Update a product
Route::put('product', [ProductController::class, 'store']);

//List products by userId
Route::get('/products/search/{searchvalue}', [ProductController::class, 'showSearched']);

// Delete a product by Id
Route::delete('product/{id}', [ProductController::class, 'destroy']);

/**
 * Upload images routing
 * source: https://www.itsolutionstuff.com/post/laravel-8-image-upload-tutorial-exampleexample.html
 */

Route::post('image-upload', [ImageUploadController::class, 'imageUpload']);
