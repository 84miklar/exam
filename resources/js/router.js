// The Vue router

import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

import Home from "./vue/Home.vue";
import About from "./vue/About.vue";
import SellProduct from "./vue/SellProduct.vue";
import PageNotFound from "./vue/404.vue";
import HemOchFritid from "./vue/HemOchFritid.vue";
import Elektronik from "./vue/Elektronik.vue";
import Fordon from "./vue/Fordon.vue";
import Login from "./vue/Login.vue";
import Logout from "./vue/Logout.vue";
import Register from "./vue/Register.vue";
import Profile from "./vue/Profile.vue";
import Admin from "./vue/Admin.vue";

import { store } from "./store.js";
const routes = new VueRouter({
    mode: "history",
    linkExactActiveClass: "active",
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
        },
        {
            path: "/about",
            name: "about",
            component: About,
        },
        {
            path: "/salj",
            name: "sälj",
            component: SellProduct,
            //User must be logged in to enter
            beforeEnter: (to, from, next) => {
                if (!store.state.userLoggedIn) next({ name: "login" });
                else next();
            },
        },
        {
            path: "/hem-och-fritid",
            name: "hemOchFritid",
            component: HemOchFritid,
        },
        {
            path: "/fordon",
            name: "Fordon",
            component: Fordon,
        },
        {
            path: "/elektronik",
            name: "elektronik",
            component: Elektronik,
        },
        {
            path: "/logga-in",
            name: "login",
            component: Login,
        },
        {
            path: "/logga-ut",
            name: "logout",
            component: Logout,
            //User must be logged in to enter
            beforeEnter: (to, from, next) => {
                if (!store.state.userLoggedIn) next({ name: "login" });
                else next();
            },
        },
        {
            path: "/registrera",
            name: "register",
            component: Register,
        },
        {
            path: "/profil",
            name: "profil",
            component: Profile,
            //User must be logged in to enter
            beforeEnter: (to, from, next) => {
                if (!store.state.userLoggedIn) next({ name: "login" });
                else next();
            },
        },
        {
            path: "/admin",
            name: "admin",
            component: Admin,
            //User must be logged in as administration to enter
            beforeEnter: (to, from, next) => {
                if (!store.state.userIsAdmin) next({ name: "home" });
                else next();
            },
        },

        {
            path: "*",
            name: "pageNotFound",
            component: PageNotFound,
        },
    ],
});

export default routes;
