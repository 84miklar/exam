<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 50) as $value) {

            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make($faker->password),
                'municipality' => $this->addMunicipality(),
                'telephone' => $faker->tollFreePhoneNumber,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
    /**
     * Help function for assigning value for the table column municipality.
     */
    private function addMunicipality()
    {
        $municipality = "Trestad";
        $randomNumber = rand(1, 3);
        $municipality = $randomNumber == 1 ?  "Vänersborg" : $municipality;
        $municipality = $randomNumber == 2 ?  "Trollhättan" : $municipality;
        $municipality = $randomNumber == 3 ?  "Uddevalla" : $municipality;
        return $municipality;
    }
}
