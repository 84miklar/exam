<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //List all users 
        $users = User::orderBy('users.id')->get();
        // Return collection of users as a resource
        return UserResource::collection($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validateUser($request);
        //Replace password with hashed password
        $password = array_search($request->password, $validated);
        $hashed = Hash::make($request->password);
        $validated[$password] = $hashed;

        //creates a new user.
        $user = User::create($validated);

        return new UserResource($user);
    }
    /**
     * Validates request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array request
     */
    private function validateUser(Request $request)
    {
        return $request->validate([
            'name' => 'required',
            'email' => ' required | email',
            'password' => 'required | min:6 | max:12 |confirmed',
            'municipality' => 'required',
            'telephone' => '',
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get a single User
        $user = User::findOrfail($id);
        return new UserResource($user);
    }

    /**
     * Updates an existing user.
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::findOrFail($request->id);
        $this->validateUser($request);

        $user->name = $request->name;

        if ($user->email !== $request->email) {

            $user->email = $request->email;
        }
        $user->password = Hash::make($request->password);
        $user->municipality = $request->municipality;
        $user->telephone = $request->telephone;

        if ($user->update()) {
            return new UserResource($user);
        }
    }

    /**
     * Login the requested user, if authenticated.
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6|max:12',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, true)) {
            return response()->json(Auth::user(), 200);
        } else {
            throw ValidationException::withMessages([
                'email' => ['Felaktiga uppgifter angivna.']
            ]);
        }
    }
    /**
     * Logout the logged in user and delete it from the session storage.
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Session::flush();
        if (Auth::logout()) {
            return response()->json(null, 200);
        }
    }
    /**
     * gets the users and their products, ordered by the most products. Admin only.
     *  @return \Illuminate\Http\Response
     */
    public function getTheMostSeller()
    {
        $users = DB::table('users')
            ->join('products', 'products.user_id', '=', 'users.id')
            ->select('users.*', DB::raw("count(products.user_id) as productCount"))
            ->groupBy('users.id')
            ->orderBy('productCount', 'desc')
            ->get();
        return $users;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find and delete user
        $user = User::findOrFail($id);
        if ($user->delete()) {
            return new UserResource($user);
        }
    }
}
