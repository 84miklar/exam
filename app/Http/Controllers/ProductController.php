<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Http\Resources\Product as ProductResource;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;



class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('user')->orderby('created_at', 'desc')->paginate(15);
        return $products;
    }

    /**
     * Display a listing of the resource in demanded category.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategory($category)
    {
        if ($category == 1 || $category == 2 || $category == 3) {
            $category = $this->setSegment($category);
            $products = Product::with('user')
                ->where('segment', $category)
                ->orderby('created_at', 'desc')
                ->paginate(15);
            return $products;
        } else {
            return response()->json("no data", 404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated =  $request->validate([
            'product' => 'required',
            'price' => ' required | integer',
            'condition' => 'required',
            'segment' => 'required',
            'description' => 'required',
        ]);
        if ($validated) {

            //Checks if the request method is 'put' and finds the requested id, else the method should be 'update' and creates a new product.
            $product = $request->isMethod('put') ? Product::findOrFail($request->product_id) : new Product;

            $product->product = $request->product;
            $product->price = $request->price;
            $product->condition = $this->setCondition($request);
            $product->segment =  $this->setSegment($request->segment);
            $product->description = $request->description;
            $product->user_id = $request->user_id;
            $product->img_URL = $request->img_URL;

            if ($product->save()) {
                return new ProductResource($product);
            }
        }
    }
    /**
     * Transforms the incoming Request condition value to a corresponding string.
     * @param \Illuminate\Http\Request  $request
     * @return string the corresponding value.
     */
    private function setCondition(Request $request)
    {
        $condition = "";
        $request->condition == '1' ? $condition = "Använt skick" : $condition;
        $request->condition == '2' ? $condition = "Bra skick" : $condition;
        $request->condition == '3' ? $condition = "Som ny" : $condition;
        return $condition;
    }

    /**
     * Transforms the incoming Request condition value to a corresponding string.
     * @param \Illuminate\Http\Request  $request
     * @return string the corresponding value.
     */
    private function setSegment($inputSegment)
    {
        $segment = "";
        $inputSegment == '1' ? $segment = "Hem och Fritid" : $segment;
        $inputSegment == '2' ? $segment = "Elektronik" : $segment;
        $inputSegment == '3' ? $segment = "Fordon" : $segment;
        return $segment;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUserProducts($userId)
    {

        //Get a single products
        return Product::where('user_id', $userId)->orderByDesc('created_at')->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $seatchValue
     * @return \Illuminate\Http\Response
     */
    public function showSearched($searchValue)
    {
        return Product::with('User')->where('product', 'LIKE', '%' . $searchValue . '%')->orderby('created_at', 'desc')->paginate(15);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find product and delete from database and delete image from image folder.
        $product = Product::findOrFail($id);
        if ($product->delete()) {
            $filePath = public_path('storage/images/') . $product->img_URL;
            File::delete($filePath);
            return new ProductResource($product);
        }
    }
}
