// Vuex, the global storage

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        user: null,
        userLoggedIn: false,
        userIsAdmin: false,
    },
    getters: {
        user(state) {
            return state.user;
        },
        logged(state) {
            return state.userLoggedIn;
        },
        isAdmin(state) {
            return state.userIsAdmin;
        },
    },
});
